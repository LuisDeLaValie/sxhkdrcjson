




import json
import sys 

contarchivo=""
contarchivo+="#      @@@@@@   @@@  @@@  @@@  @@@  @@@  @@@  @@@@@@@   \n"
contarchivo+="#     @@@@@@@   @@@  @@@  @@@  @@@  @@@  @@@  @@@@@@@@  \n"
contarchivo+="#     !@@       @@!  !@@  @@!  @@@  @@!  !@@  @@!  @@@  \n"
contarchivo+="#     !@!       !@!  @!!  !@!  @!@  !@!  @!!  !@!  @!@  \n"
contarchivo+="#     !!@@!!     !@@!@!   @!@!@!@!  @!@@!@!   @!@  !@!  \n"
contarchivo+="#      !!@!!!     @!!!    !!!@!!!!  !!@!!!    !@!  !!!  \n"
contarchivo+="#          !:!   !: :!!   !!:  !!!  !!: :!!   !!:  !!!  \n"
contarchivo+="#         !:!   :!:  !:!  :!:  !:!  :!:  !:!  :!:  !:!  \n"
contarchivo+="#     :::: ::    ::  :::  ::   :::   ::  :::   :::: ::  \n"
contarchivo+="#     :: : :     :   ::    :   : :   :   :::  :: :  :   \n"
contarchivo+="\n\n\n"


def Modulo(modulo):
    long=56-(len(modulo)+4)
    tex="#::::::::::::::::::::::::::::::::::::::::::::::::::::::#\n#"
    i=1
    while i < long:
        if (int(long/2)) == i:
            tex+="  "+modulo+"  "
        else:
            tex+=":"
        i += 1  
    tex+="#\n"
    tex+="#::::::::::::::::::::::::::::::::::::::::::::::::::::::#\n\n\n"
    return tex


def Comando(comando):
    long=52-(len(comando)+4)
    tex="#"
    i=1
    while i < long:
        if (int(long/2)) == i:
            tex+="  "+comando+"  "
        else:
            tex+="_"
        i += 1  
    tex+="#\n\n"
    return tex


with open('config.json') as file:
    data = json.load(file)

    for modulo in data.keys(): 
        contarchivo+= Modulo(modulo)       
        jsonModulo=data.get(modulo, '')

        for comandos in jsonModulo.keys():
            contarchivo+= Comando(comandos)
            jsonComando=jsonModulo.get(comandos, '')
            contarchivo+="#"+jsonComando.get('des', '')+"\n"
            contarchivo+=jsonComando.get('keys', '')+"\n"
            contarchivo+="\t"+jsonComando.get('comando', '')+"\n"
            
            
        contarchivo+="\n\n\n"
    fic = open("sxhkdrc", "w")
    fic.write(contarchivo)
    fic.close()